import { Selector, t } from 'testcafe';
import { basePage } from './base.page';

const newUser = require('../config/newUser.json');

export const QuotePage = {
    //url: `${basePage.url}`,


    // Elements
    //heading: Selector("h1[class='compass-progress-title']"),
    heading: Selector('h1').withText('Quote'),
    goodsdescription: Selector('#compass_goodsDescription'),
    // continue: Selector('button').withExactText('Continue'),
    // endSend: Selector('button').withExactText('End & Send'),
    continue: Selector("button[data-test-id='compass-quote-form-submit']"),
    endSend: Selector("button[data-test-id='compass-quote-form-end-and-send']"),

    async headingText(): Promise<string> { return await this.heading.innerText },

    async clickContinue() { await t.click(this.continue) },
    async clickEndSend() { await t.click(this.endSend) }

    
};