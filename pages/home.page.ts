import { Selector, t } from 'testcafe';
import { basePage } from './base.page';

const newUser = require('../config/newUser.json');

export const homePage = {
    url: `${basePage.url}`,
    // Elements
    retailer:Selector('#login-retailer'),
    Username:Selector('#login-username'),
    Password:Selector('#login-password'),
    //loginButton: Selector('button').withText('Sign in'),
    loginButton: Selector("button[data-test-id='compass-login-submit']"),
    //loginButton: Selector("button[text='Sign in']"),
    


    async enterRetailer() { await t.typeText(this.retailer, newUser.Retailer)},

    async enterUsername() { await t.typeText(this.Username, newUser.Username)},

    async enterPassword() { await t.typeText(this.Password, newUser.Password)},

    async clickLogin() { await t.click(this.loginButton) }
};