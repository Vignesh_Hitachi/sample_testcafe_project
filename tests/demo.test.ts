import { axeCheck, createReport } from 'axe-testcafe';

import { getRandomEmail } from '../utils/utils';
import { loginMock } from '../mocks/login.mock';
import { homePage } from '../pages/home.page';
import { QuotePage } from '../pages/Quote.Page';


fixture('Testing CM3 Retail Website')
    .page(homePage.url);

test('Create an account', async (t) => {
    await t.maximizeWindow();
    await homePage.enterRetailer();
    await homePage.enterUsername();
    await homePage.enterPassword();
    await homePage.clickLogin();
    t.expect(await QuotePage.goodsdescription.exists);
    t.expect(await QuotePage.heading.exists);

    t.expect(await QuotePage.continue.exists);
    t.expect(await QuotePage.endSend.exists);
   
    await QuotePage.clickContinue();

    t.takeScreenshot;
    // await signInPage.createNewAccount(getRandomEmail());
    // await registrationPage.registerWithDetails();
    // await t.expect(await myAccountPage.getAccountButtonText()).eql('Test Tester')    
});

